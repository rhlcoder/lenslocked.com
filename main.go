package main

import (
	"fmt"
	"net/http"
)

func handlerFunc(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<h1>Vamooo, me funciona el server de golang!!!</h1>")
}

func main() {
	http.HandleFunc("/", handlerFunc)
	http.ListenAndServe(":3000", nil) //localhost:3000 esta umplicito si no se pone nada. y nil significa que se usa el handler por defecto
}
